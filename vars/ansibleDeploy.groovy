def call(){
  sh "ansible-galaxy install -r content/collections/requirements.yml"
  sh "sudo chmod +x inventory/HCV_GetConnection.py"
  sh "export ANSIBLE_CONFIG=ansible.cfg"
  sh "python3.9 inventory/HCV_GetConnection.py"
  sh "ansible-playbook  main.yml"
}